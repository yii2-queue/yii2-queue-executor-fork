<?php
$config = [
    'id' => 'yii2-queue-app',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(dirname(__DIR__))) . '/vendor',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql;dbname=mysql_database',
            'username' => 'root',
            'password' => 'mysql_strong_password',
            'charset' => 'utf8',
        ],
    ],
];

return $config;
