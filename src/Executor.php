<?php

namespace yii\queue\executors\fork;

use \Exception;
use Kraken\Promise\Promise;
use React\EventLoop\Timer\TimerInterface;
use Spork\Fork;
use Spork\ProcessManager;

/**
 */
class Executor extends \yii\queue\executors\Executor
{
    private $_manager;

    /**
     * Gets the manager.
     *
     * @return     ProcessManager  The spork proccess manager.
     */
    public function getManager()
    {
        if (!$this->_manager) {
            $this->_manager = new ProcessManager();
        }
        return $this->_manager;
    }

    /**
     * @inheritdoc
     */
    public function handleMessage($message, $id = null, $ttr = null, $attempt = null)
    {
        $promise = new Promise(function ($resolve, $reject) use (&$message) {
            $serializer = $this->getQueue()->getSerializer();
            $fork = $this->getManager()->fork(function () use ($message, $serializer) {
                // Really ugly but new connection is needed for fork
                $connection = \Yii::$app->getDb();
                $connection->close();
                $connection->open();

                $job = $serializer->unserialize($message);

                return $job->execute();
            })->then(function (Fork $fork) use (&$resolve) {
                call_user_func($resolve, $fork->getResult());
            }, function (Fork $fork) use (&$reject) {
                /**
                 * @var        \Spork\Util\Error
                 */
                $error = $fork->getError();
                $className = $error->getClass();

                /**
                 * @var Exception
                 */
                $exception = new Exception($error->getMessage(), $error->getCode());
                call_user_func($reject, $exception);
            });

            $this->forkTimer($fork);
        });
        $connection = \Yii::$app->getDb();
        $connection->close();
        $connection->open();
        return $promise;
    }

    /**
     * @param      \Spork\Fork  $fork   The fork
     *
     * @return     TimerInterface
     */
    protected function forkTimer(Fork $fork)
    {
        return $this->getQueue()->getEventLoop()->addPeriodicTimer(1, function (TimerInterface $timer) use ($fork) {
            if ($fork->wait(false) && $fork->isExited()) {
                $timer->cancel();
            }
        });
    }
}
